import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Route, BrowserRouter as Router } from 'react-router-dom'
import Users from './router/Users'
import Contact from './router/Contact'
import NavigationBar from './navigation/Navigation';

const routing = (
    <div>
        <Router>
            <div>
                <NavigationBar />
                <Route exact path="/" component={App}/>
                <Route path="/users" component={Users} />
                <Route path="/login" component={Contact}/>
            </div>
        </Router>
    </div>
)
ReactDOM.render(routing, document.getElementById('root'))

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();

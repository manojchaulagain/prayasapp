import React, {Component} from 'react';
import { Table } from 'reactstrap';

class CountryTable extends Component {
    render() {
        const rowItems = this.props.countries.map((country) => {
            return <tr key={country.alpha3Code}>
                <th scope="row">{country.alpha3Code}</th>
                <td>{country.name}</td>
                <td>{country.alpha2Code}</td>
                <td>{country.capital}</td>
            </tr>            
        });
        return (
            <Table bordered striped>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Alpha2Code</th>
                        <th>Capital</th>
                    </tr>
                </thead>
                <tbody>
                    {rowItems}
                </tbody>
            </Table>
        );
    }
}

export default CountryTable;
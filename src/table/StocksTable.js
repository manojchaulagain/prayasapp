import React, {Component} from 'react';
import { Table, Row } from 'reactstrap';
import StockCard from '../card/StockCard';

class StocksTable extends Component {
    render() {                
        const cardItems = this.props.stocks.slice(1, 20).map((stock) => {
            return <Row key={stock.id}><StockCard key={stock.id} stock={stock}/>{' '}</Row>;
        });

        const rowItems = this.props.stocks.slice(1, 20).map((stock) => {
            return <tr key={stock.id}>
                <th scope="row">{stock.symbol}</th>
                <td>{stock.name}</td>
                <td>{stock.lastSale}</td>
                <td>{stock.marketCap}</td>
                <td>{stock.adrTso}</td>
                <td>{stock.ipoYear}</td>
                <td>{stock.sector}</td>
                <td>{stock.industry}</td>
                {/* <td>{stock.summaryQuote}</td> */}
            </tr>            
        });
        return (
            <div>
            {cardItems}
            <Table bordered striped>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>LastSale</th>
                        <th>Market Cap</th>
                        <th>ADR TSO</th>
                        <th>IPO Year</th>
                        <th>Sector</th>
                        <th>Industry</th>
                        {/* <th>Summary Quote</th> */}
                    </tr>
                </thead>
                <tbody>
                    {rowItems}
                </tbody>
            </Table>
            </div>
        );
    }
}

export default StocksTable;
import React, { Component } from 'react';
import {
    Container, Col, Form,
    FormGroup, Label, Input,
    Button, Row
} from 'reactstrap';
import {commonApi} from '../Api';
import AuthService from '../AuthService';

class Contact extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            username: "",
            password: "",
            auth: new AuthService()
        };
        this.onClicked = this.onClicked.bind(this);
        
    }
    
    componentWillMount() {
        if(this.state.auth.loggedIn()) {
            this.props.history.replace('/');
        }
    }
    onClicked(username, password) {
            this.state.auth.login(username, password);
        }

        render() {
        return (
            <Container>
                <Row>
                    <h2>Sign In</h2>
                </Row>
                <Row>
                    <Form className="form">
                        <Col>
                            <FormGroup>
                                <Label>Username</Label>
                                <Input
                                    type="text"
                                    name="username"
                                    id="username"
                                    placeholder="Username"
                                    value={this.state.username}
                                    onChange={(event) => this.setState({
                                        username: event.target.value
                                    })}
                                />
                            </FormGroup>
                        </Col>
                        <Col>
                            <FormGroup>
                                <Label for="examplePassword">Password</Label>
                                <Input
                                    type="password"
                                    name="password"
                                    id="examplePassword"
                                    placeholder="********"
                                    value={this.state.password}
                                    onChange={(event) => this.setState({
                                        password: event.target.value
                                    })}
                                />
                            </FormGroup>
                        </Col>
                        <Button onClick={() => this.onClicked(this.state.username, this.state.password)}>Submit</Button>
                    </Form>
                </Row>
            </Container>
        );
    }
}

export default Contact;
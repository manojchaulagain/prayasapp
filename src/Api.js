import axios from 'axios';
import { Component } from 'react';

class Api extends Component {

    constructor() {
        super();
        this.state = {
            instance: axios.create({
                baseURL: 'http://localhost:8080/api/',
                timeout: 1000,
                headers: {}
            }),
            endpoints: {}
        }
        this.createBasicEndPoints();
    }

    createBasicEndPoints() {
        var ep = {};
        ep.get = (query, headers) => this.state.instance(query, headers).then(response => Promise.resolve(response)).catch(error => Promise.reject(error));
        this.setState({
            endpoints: ep
        });
    }

    getter() {
        return this.state.endpoints.get;
    }
}
export const commonApi = axios.create({
    baseURL: 'http://localhost:8080/api/',
    timeout: 1000,
    headers: {}
});

export default Api;
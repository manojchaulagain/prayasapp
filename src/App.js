import React, { Component } from 'react';
// import logo from './logo.svg';
import './App.css';
import { Button, Container, Row, Spinner, Col } from 'reactstrap';
// import NavigationBar from './navigation/Navigation';
import AppCarousel from './carousel/Carousel';
import CountryTable from './table/CountryTable';
// import axios from 'axios';
import { commonApi } from './Api';
import StocksTable from './table/StocksTable';
import AuthService from './AuthService';

class App extends Component {

  constructor() {
    super();
    this.state = {
      token: "",
      tokenType: "",
      loaded: false,
      loadedStocks: false,
      loading: false,
      loadingStocks: false,
      countries: [],
      stocks: [],
      fadeIn: true,
      auth: new AuthService(),
      loggedIn: false

    }
    this.onLogin = this.onLogin.bind(this);
    this.onLoad = this.onLoad.bind(this);
    this.loadStocks = this.loadStocks.bind(this);
  }

  // async componentDidMount() {
  //   await this.onLogin("menuka.dangal", "N1e2p3al!");
  //   await this.onLoad();
  //   await this.loadStocks();
  // }

  async onLoad() {
    this.setState({
      loading: true,
      loaded: false,
      countries: []
    });
    var headers = {
      headers: {
        Authorization: this.state.auth.getBearerToken()
      }
    }
    try {
      var countries = (await commonApi.get("countries", headers)).data;
      this.setState({
        loaded: true,
        countries: countries,
        loading: false
      });
    } catch (error) {
      console.log("Could not find login data" + error);
    }
  }

  async loadStocks() {
    this.setState({
      loadingStocks: true,
      stocks: [],
      loadedStocks: false
    });
    var headers = {
      headers: {
        Authorization: this.state.auth.getBearerToken()
      }
    }
    try {
      var stocks = (await commonApi.get("stocks", headers)).data;
      this.setState({
        loadedStocks: true,
        stocks: stocks,
        loadingStocks: false
      });
    } catch (error) {
      console.log("Could not find login data" + error);
    }
  }

  async onLogout() {
    await this.state.auth.logout();
    this.setState({
      loggedIn: this.state.auth.loggedIn(),
      countries: [],
      stocks: [],
      loaded: false,
      loadedStocks: false
    });
  }

  async onLogin(username, password) {
    await this.state.auth.login(username, password);
    this.setState({
      loggedIn: this.state.auth.loggedIn()
    });
  }

  render() {
    return (
      <div className="App">
        {/* <NavigationBar/> */}
        {/* <Container>
          <Row>
            <AppCarousel></AppCarousel>
          </Row>
        </Container> */}
        <Container>
          <Row>
            <Col>
              {!this.state.loggedIn && <Button className="rightFloat btnMargin" color="danger" disabled={this.state.loggedIn} onClick={() => this.onLogin("menuka.dangal", "N1e2p3al!")}>Sign In</Button>}{' '}
              {this.state.loggedIn && <Button className="rightFloat btnMargin" color="success" disabled={!this.state.loggedIn} onClick={() => this.onLoad()}>Load Countries</Button>}{' '}
              {this.state.loggedIn && <Button className="rightFloat btnMargin" color="info" disabled={!this.state.loggedIn} onClick={() => this.loadStocks()}>Load Stocks</Button>}{' '}
              {this.state.loggedIn && <Button className="rightFloat btnMargin" color="warning" disabled={!this.state.loggedIn} onClick={() => this.onLogout()}>Sign Out</Button>}{' '}
            </Col>
          </Row>
          <Row style={{ paddingTop: '25px', paddingBottom: '25px' }}>
            <Col>
              {this.state.loaded && <CountryTable countries={this.state.countries} />}
              {this.state.loading && <Spinner style={{ width: '3rem', height: '3rem' }} color="primary" />}
            </Col>
          </Row>
          <Row style={{ paddingBottom: '25px' }}>
            <Col>
              {this.state.loadedStocks && <StocksTable stocks={this.state.stocks} />}
              {this.state.loadingStocks && <Spinner style={{ width: '3rem', height: '3rem' }} color="primary" type="grow" />}
            </Col>
          </Row>
        </Container>

      </div>
    );
  }
}

export default App;
